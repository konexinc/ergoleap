# -*- coding: utf-8 -*-

import pypot.robot
import time
from numpy import *
from KineErgo import MGD
import Leap
import sys

c = Leap.Controller()
ergo = pypot.robot.from_json('ergo.json')

state = 0
dt = 0.01
t0 = time.clock()
speed = array([10.0,10.0,10.0,10.0,10.0])
Tbreath = 2.0
Abreath = 1.0
qmin = array([-100,-45,-90,-100,-90])
qmax = array([100,90,110,100,45])
seeHand = 0

def cost(X,Xc):
    d = pow(linalg.norm(X-Xc,axis=0),2)
    return d
    
while True:
    t = time.clock()
    # sensors
    temp = 0
    qm = array([0,0,0,0,0])
    for imot in range(len(ergo.arm)):
        mot = ergo.arm[imot]
        temp = max(temp,mot.present_temperature)
        qm[imot] = mot.present_position
    breath = Abreath*sin(2.0*pi*t/Tbreath)
    frame = c.frame()
    Xc = array([0.155,0.0,0.0])
    pitch = 0.0
    yaw = 0.0
    seeHand = 0
    pinch = 0.0
    for f in frame.fingers:
        if f.type == f.TYPE_INDEX:
            h = f.hand
            pinch = h.pinch_strength
            xD = 0.155-4*h.palm_position.z/1000.0
            yD = 4*h.palm_position.x/1000.0
            zD = -0.2+2*h.palm_position.y/1000.0
            Xc = array([xD,yD,zD])
            yaw = arctan(f.direction.x/f.direction.z)*180/pi
            pitch = arctan(f.direction.y/f.direction.z)*180/pi
            seeHand = 1
    
    if state == 0:
    # sleeping
        speed = array([10.0,10.0,10.0,10.0,10.0])
        q = array([NaN,NaN,NaN,NaN,NaN])
        if seeHand and temp<50:
            t0 = t
            state = 2

    elif state == 1:
        speed = array([40.0,40.0,20.0,20.0,20.0])
        q = array([0.0,67.0,103.0,0.0,9.0])
        if t-t0>5:
            if not seeHand:
                state = 0
            else:
                t0 = t
                if pinch <0.5:
                    state = 2
                else:
                    state = 3
        if temp > 55:
            state = 0

    elif state == 2:
        speed = array([200.0,200.0,200.0,500.0,500.0])
        K = -500.0
        a = MGD(qm)
        X = a["C"]["position"]
        J = a["C"]["jacobian"]
        # print "X"
        # print X
        # print "Xc"
        # print Xc
        q = qm+K*(dot((X-Xc),J))
        q[2] = 90-q[1]
        q[3] = 4*yaw+q[0]
        q[4] = pitch
        if not seeHand or temp> 55:
            t0 = t
            state = 1
        

    for imot in range(len(ergo.arm)):
        ergo.arm[imot].moving_speed = speed[imot]
        
        if isnan(q[imot]):
            ergo.arm[imot].compliant = True
        else:
            ergo.arm[imot].compliant = False
            if q[imot]>qmax[imot]:
                q[imot]=qmax[imot]
            if q[imot]<qmin[imot]:
                q[imot]=qmin[imot]
            ergo.arm[imot].goal_position = q[imot]
    ergo.By.goal_position = ergo.By.goal_position+breath
    sys.stdout.write("\rTemperature : %fdegC" % temp)
    sys.stdout.write(" - Angles :" + str(qm))
    sys.stdout.write(" - Etat :" + str(state))
    sys.stdout.write(" - Pinch :" + str(pinch))
    sys.stdout.flush()
    time.sleep(dt)
    #print time.clock()-t
        

from numpy import *
import time,sys


def rot_mat(axe,s,c):
    if axe == "x":
        return array([[1,0,0],[0,c,-s],[0,s,c]])
    elif axe == "y":
        return array([[c,0,s],[0,1,0],[-s,0,c]])
    elif axe == "z":
        return array([[c,-s,0],[s,c,0],[0,0,1]])
    else:
        return 0

def cross_prod(a,b):
    ax = array([[0,-a[2],a[1]],[a[2],0,-a[0]],[-a[1],a[0],0]])
    return dot(ax,b)

def MGD(q):
    # q shape
    # q[0] : Az
    # q[1] : Ay
    # q[2] : By
    # q[3] : Cx
    # q[4] : Dy
    
    # geometric data (m)
    # Length of arm AB
    LAB = 0.083
    # Length of arm BC
    LBC = 0.072
    # Length of arm CD
    LCD = 0.062
    # Length of arm DE
    LDE = 0.030
    articulationNames = ["Az","Ay","By","Cx","Dy"]

    # dictionnary of points
    points = {}
    points["A"]={}
    points["B"]={}
    points["C"]={}
    points["D"]={}
    points["E"] = {}
    # segments of the robot
    segments = {}
    segments["AB"] = array([0,0,0])
    segments["BC"] = array([0,0,0])
    segments["CD"] = array([0,0,0])
    segments["DE"] = array([0,0,0])
    
    #definition of axes
    x=array([1,0,0])
    y=array([0,1,0])
    z=array([0,0,1])

    # computation of sin and cos
    s = sin(q*pi/180.0)
    c = cos(q*pi/180.0)

    # left arm chain
    rot0=rot_mat("z",s[0],c[0])
    rot1=rot_mat("y",s[1],c[1])
    rot2=rot_mat("y",s[2],c[2])
    rot3=rot_mat("x",s[3],c[3])
    rot4=rot_mat("y",s[4],c[4])
    t0 = rot0
    t1 = dot(t0,rot1)
    t2 = dot(t1,rot2)
    t3 = dot(t2,rot3)
    t4 = dot(t3,rot4)
    
    segments["AB"] = dot(t1,z)*LAB
    segments["BC"] = dot(t2,z)*LBC
    segments["CD"] = -dot(t3,x)*LCD
    segments["DE"] = -dot(t4,x)*LDE
    
    points["A"]["position"] = array([0.0,0.0,0.0])
    points["B"]["position"] = points["A"]["position"]+segments["AB"]
    points["C"]["position"] = points["B"]["position"]+segments["BC"]
    points["D"]["position"] = points["C"]["position"]+segments["CD"]
    points["E"]["position"] = points["D"]["position"]+segments["DE"]
       
    points["B"]["jacobian"] = zeros((3,5))
    points["B"]["jacobian"][:,0] = cross_prod(dot(t0,z),segments["AB"])
    points["B"]["jacobian"][:,1] = cross_prod(dot(t1,y),segments["AB"])
    
    points["C"]["jacobian"] = zeros((3,5))
    points["C"]["jacobian"][:,0] = cross_prod(dot(t0,z),segments["AB"]+segments["BC"])
    points["C"]["jacobian"][:,1] = cross_prod(dot(t1,y),segments["AB"]+segments["BC"])
    points["C"]["jacobian"][:,2] = cross_prod(dot(t2,y),segments["BC"])
    
    points["D"]["jacobian"] = zeros((3,5))
    points["D"]["jacobian"][:,0] = cross_prod(dot(t0,z),segments["AB"]+segments["BC"]+segments["CD"])
    points["D"]["jacobian"][:,1] = cross_prod(dot(t1,y),segments["AB"]+segments["BC"]+segments["CD"])
    points["D"]["jacobian"][:,2] = cross_prod(dot(t2,y),segments["BC"]+segments["CD"])
    points["D"]["jacobian"][:,3] = cross_prod(dot(t3,x),segments["CD"])
    
    points["E"]["jacobian"] = zeros((3,5))
    points["E"]["jacobian"][:,0] = cross_prod(dot(t0,z),segments["AB"]+segments["BC"]+segments["CD"]+segments["DE"])
    points["E"]["jacobian"][:,1] = cross_prod(dot(t1,y),segments["AB"]+segments["BC"]+segments["CD"]+segments["DE"])
    points["E"]["jacobian"][:,2] = cross_prod(dot(t2,y),segments["BC"]+segments["CD"]+segments["DE"])
    points["E"]["jacobian"][:,3] = cross_prod(dot(t3,x),segments["CD"]+segments["DE"])
    points["E"]["jacobian"][:,4] = cross_prod(dot(t4,y),segments["DE"])
    
    return points

if __name__ == "__main__":
    t1 = time.clock()
    X=MGD(array([0,0,90,0,0]))
    t2 = time.clock()
    print "Time of kinematics computation in s:"
    print (t2-t1)
    print X["E"]
    
    
    
